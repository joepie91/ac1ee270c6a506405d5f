module.exports = function(array) {
	return array.reduce(function(total, subArray) {
		subArray.forEach(function(item) {
			total.push(item);
		});
		
		return total;
	}, []);
}